<?php

/**
 * @file
 * Rules integration for the Nodeaccess module.
 */

/**
 * Implements hook_rules_data_info().
 */
function rules_nodeaccess_rules_data_info() {
  return array(
    'individual_node_grants' => array(
      'label' => t('Individual Node Grants'),
    ),
  );
}

/**
 * Implements hook_rules_event_info().
 */
function rules_nodeaccess_rules_event_info() {
  $items = array();
  $items['rules_nodeaccess_grants_form_submitted'] = array(
    'group' => t('Nodeaccess'),
    'label' => t('After submitting grants form on node grants tab'),
    'variables' => array(
      'node' => array(
        'type' => 'node',
        'label' => t('Node on which grants are updated.'),
      ),
      'individual_node_grants' => array(
        'type' => 'individual_node_grants',
        'label' => t('Individual Node Grants'),
      ),
    ),
  );
  $items['rules_nodeaccess_grants_form_being_built'] = array(
    'group' => t('Nodeaccess'),
    'label' => t('Grants form on node grants tab is being built'),
    'variables' => array(
      'nid' => array(
        'type' => 'integer',
        'label' => t('Node ID for which grants form is displayed.'),
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_rules_action_info().
 */
function rules_nodeaccess_rules_action_info() {
  $items = array(
    '_rules_nodeaccess_overwrite_node_grants' => array(
      'label' => t('Overwrite node grants'),
      'parameter' => array(
        'individual_node_grants' => array(
          'type' => 'individual_node_grants',
          'label' => t('Individual Node Grants'),
        ),
        'nid' => array(
          'type' => 'integer',
          'label' => t('Node NID to overwrite grants on'),
        ),
      ),
      'group' => t('Nodeaccess'),
    ),
  '_rules_nodeaccess_fetch_node_grants' => array(
      'label' => t('Fetch node grants'),
      'parameter' => array(
        'nid' => array(
          'type' => 'integer',
          'label' => t('Node NID to fetch grants'),
        ),
      ),
    'provides' => array(
        'individual_node_grants' => array(
          'type' => 'individual_node_grants',
          'label' => t('Individual Node Grants'),
        ),
    ),
      'group' => t('Nodeaccess'),
    ),
  );
  return $items;
}

/**
 * Private function to overwrite grants of node.
 */
function _rules_nodeaccess_overwrite_node_grants($individual_node_grants, $nid) {
  // Writing this function as API is not available from Nodeaccess.
  $form_values = $individual_node_grants['values'];
  $grants = array();
  $node = node_load($nid);
  if ($node != FALSE) {
    foreach (array('uid', 'rid') as $type) {
      $realm = 'nodeaccess_' . $type;
      if (isset($form_values[$type]) && is_array($form_values[$type])) {
        foreach ($form_values[$type] as $gid => $line) {
          $grant = array(
            'gid' => $gid,
            'realm' => $realm,
            'grant_view' => empty($line['grant_view']) ? 0 : $line['grant_view'],
            'grant_update' => empty($line['grant_update']) ? 0 : $line['grant_update'],
            'grant_delete' => empty($line['grant_delete']) ? 0 : $line['grant_delete'],
          );
          if ($grant['grant_view'] || $grant['grant_update'] || $grant['grant_delete']) {
            $grants[] = $grant;
          }
        }
      }
      node_access_write_grants($node, $grants, $realm);
    }

    // Save role and user grants to nodeaccess table.
    db_delete('nodeaccess')
      ->condition('nid', $nid)
      ->execute();
    foreach ($grants as $grant) {
      db_insert('nodeaccess')
        ->fields(array(
          'nid' => $nid,
          'gid' => $grant['gid'],
          'realm' => $grant['realm'],
          'grant_view' => $grant['grant_view'],
          'grant_update' => $grant['grant_update'],
          'grant_delete' => $grant['grant_delete'],
        ))
        ->execute();
    }
  }
  else {
    watchdog('rules_nodeaccess', 'Unable to find node with nid @nid to overwrite grants.', array('@nid' => $nid), WATCHDOG_ERROR);
  }
}

/**
 * Private function to fetch node grants.
 */
function _rules_nodeaccess_fetch_node_grants($nid) {
  $node = node_load($nid);
  $individual_node_grants = nodeaccess_get_grants($node);
  return array('individual_node_grants' => array("values" => $individual_node_grants));
}
