INTRODUCTION
------------

The Rules Nodeaccess module provides following events and actions for
rules module. This can be used to replicate per node grants from one node to
another node. This is very helpful if you are using node reference and entity 
inline form. Grants can be easily copied from parent node to referred nodes.

Events
 * After submitting grants form on node grants tab

Actions 
 * Overwrite node grants

 * For a full description of the module, visit the project page:
   https://drupal.org/project/rules_nodeaccess

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/rules_nodeaccess

REQUIREMENTS
------------

This module requires the following modules:

 * Nodeaccess (https://drupal.org/project/nodeaccess)
 * Rules (https://drupal.org/project/rules)

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will provide additional events and actions for rules module.

MAINTAINERS
-----------

Current maintainers:
 * Devendra Jadhav (jadhavdevendra) - https://www.drupal.org/user/485984
